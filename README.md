# Functions to create and validate Finnish bank reference checksum digit

Javascript functions to create and validate Finnish bank reference codes.

## Create checksum digit

Function calculateBankReferenceCodeCheckSum(refBase, checksumDigitOnly) calculates the reference digit for the base reference code

Where

- refBase = base code of the bank transfer code 
- checksumDigitOnly = true | false, true means that the return value is only the checksum digit and false that it is the whole reference code

### Return values

- Reference code with checksum digit when checksumDigitOnly = false or missing
- Check sum digit as string when checksumDigitOnly = true
- False (boolean) if error in refBase code string ([0-9 ][0-9 ]+)

### Examples

```
calculateBankReferenceCodeCheckSum("5013890001")
```
Returns "50138900019"

```
calculateBankReferenceCodeCheckSum("5013 890 001")
```
Returns "50138900019"

```
calculateBankReferenceCodeCheckSum("5013890001", false)
```
Returns "50138900019"

```
calculateBankReferenceCodeCheckSum("5013890001", true)
```
Returns "9"

```
calculateBankReferenceCodeCheckSum("5013-890 001")
```
Returns false

## Validate a reference code

Function validateReferenceCodeCheckSum(ref) validates that the reference code has a correct format ([0-9 ][0-9 ]+) and checksum digit

Where

ref = Reference code as string, min length of code is 2

### Return values

True = reference code has a correct checksum digit
False = reference code does not have a correct checksum digit

### Examples

```
validateReferenceCodeCheckSum("50138900019")
```
Returns true

```
validateReferenceCodeCheckSum(" 50 1389 00019 ")
```
Returns true

```
validateReferenceCodeCheckSum("50138900018")
```
Returns false

```
validateReferenceCodeCheckSum("501389-00019")
```
Returns false
