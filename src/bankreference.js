//
// Copyright (C) Mika Wenell
//
// Javascript functions to create and validate Finnish bank reference codes.
//

//
// calculateChecksumDigitOfBankReferenceCode(refBase, checksumDigitOnly)
// Calculates the reference digit
// for the base code of bank transfer code
//
// Where
//
// refBase = base code of the bank transfer code 
// checksumDigitOnly = true | false, 
// If the return value is only the checksum digit or full reference string
// 
// Return values:
// Reference code with checksum digit when checksumDigitOnly = false or missing
// Check sum digit as string when checksumDigitOnly = true
// False (boolean) if error in refBase code string ([0-9 ]+)
//
// Examples:
// calculateChecksumDigitOfBankReferenceCode("5013890001") = "50138900019"
// calculateChecksumDigitOfBankReferenceCode("5013 890 001") = "50138900019"
// calculateChecksumDigitOfBankReferenceCode("5013890001", false) = "50138900019"
// calculateChecksumDigitOfBankReferenceCode("5013890001", true) = "9"
// calculateChecksumDigitOfBankReferenceCode("5013-890 001") = false

function calculateChecksumDigitOfBankReferenceCode(refBase, checksumDigitOnly)
{
    checksumDigitOnly = checksumDigitOnly | false;
    if(typeof refBase != 'string' || refBase.length < 1) return false;
    let multipliers = [ 7, 3, 1 ];
    let multiplierIndex = 0;
    let checkDigitSum = 0;
    let returnRefBase = '';
    const digitRegexp = /[0-9 \-]/;
    for (let i = refBase.length - 1; i >= 0; i--)
    {
        let digit = refBase.charAt(i);
        if(digit >= '0' && digit <= '9') {
            checkDigitSum += parseInt(refBase.charAt(i)) * multipliers[multiplierIndex];
            multiplierIndex = (multiplierIndex + 1) % multipliers.length;
            returnRefBase = digit + returnRefBase;
        } else {
            if(!digitRegexp.test(digit)) return false;
        }
    }
     return (checksumDigitOnly ? '' : returnRefBase) + ((10 - (checkDigitSum % 10)) % 10);
}

//
// validateReferenceCodeCheckSumOfBankCode(ref)
// Validates that the reference code has a correct format ([0-9 ][0-9 ]+) and checksum digit
//
// Where
//
// ref = Reference code as string, min length of code is 2
//
// Return values:
// True = reference code has a correct checksum digit
// False = reference code does not have a correct checksum digit
//
// Examples:
// validateReferenceCodeCheckSumOfBankCode("50138900019") = true
// validateReferenceCodeCheckSumOfBankCode(" 50 1389 00019 ") = true
// validateReferenceCodeCheckSumOfBankCode("50138900018") = false
// validateReferenceCodeCheckSumOfBankCode("501389-00019") = false
//

function validateReferenceCodeCheckSumOfBankCode(ref) {
    if(typeof ref != 'string') return false;
    let refTrimmed = ref.trim();
    if(refTrimmed.length < 2) return false;
    let refCheckSumDigit = refTrimmed.charAt(refTrimmed.length - 1);
    let refBase = refTrimmed.substring(0, refTrimmed.length - 1);
    return(refCheckSumDigit === calculateChecksumDigitOfBankReferenceCode(refBase, true));
}
